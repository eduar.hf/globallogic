package edu.global.logic.demo.jwt;

import java.util.Arrays;
import java.util.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

public class JSONEncodingTest {

	private static final String SHORT_BASE64_ENCODED_STRING = "A/A=";

    private static final String LONG_BASE64_ENCODED_STRING = //
            "eyJ0b3BpYyI6InRlc3QvdG9waWMvZm9yL2V2ZW50cy90b3BpYzAwMDE5L0U1ZjgiLCJwcm9wZXJ0aWVzIjp7ImNvbS5xaXZpY29uLnNlcnZpY2VzLnJlbW90ZS5ldmVudC5zZXF1ZW5jZS5udW1iZXIiOjM2OCwiZXZlbnQudG9waWNzIjoidGVzdC90b3BpYy9mb3IvZXZlbnRzL3RvcGljMDAwMTkvRTVmOCIsInVuaWNvZGUuc3RyaW5nIjoiZ0JBNVBUZjlhZ2d5aEFHdF/DnFpWZ1U3VMOWSi1hUHpow5ZoWlZOUmRMZVNrMUIxejRoX3BNd21WZ05Qw6RaSEVlcnd5NE9CRUJFRGFFRcOEdFVVNHJHM3VUw5xTSjnigqwwaTR6VVB2UnFqdlBZw4TDljNqbsOfaEFqNjc0X3U0ccO2TFFIeEZZSSJ9fQ";

    private static final String JSON_SHORT = "{ \"data\" : \"" + SHORT_BASE64_ENCODED_STRING + "\"}";
    private static final String JSON_LONG = "{ \"data\" : \"" + LONG_BASE64_ENCODED_STRING + "\"}";

    public static class DataWrapper {
        public byte[] data;
    }

    public static void main(String[] args) throws Exception {
        decode(SHORT_BASE64_ENCODED_STRING, JSON_SHORT);
        decode(LONG_BASE64_ENCODED_STRING, JSON_LONG);
    }

    public static void decode(final String base64, final String json) throws Exception {
        final byte[] bytes = Base64.getDecoder().decode(base64);
        System.out.println(Arrays.toString(bytes));

        final ObjectMapper mapper = new ObjectMapper();

        final ObjectReader textReader = mapper.readerFor(DataWrapper.class);

        final DataWrapper jsonResult = textReader.readValue(json);
        System.out.println(Arrays.equals(bytes, jsonResult.data));

    }

}
