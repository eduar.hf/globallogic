-- Inicializamos la BD

INSERT INTO Phone (id, number, citycode, contrycode) VALUES (1, '1234567', '1', '57');

INSERT INTO Usuario (id, email, name, username, password, token, isactive) VALUES (1, 'eduar.hf@gmail.com','Eduardo Espinoza', 'admin', '$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.', 'eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzI3Mjk5NDYsImlzcyI6Imh0dHA6Ly93d3cudGVzbGFpdC5jbC8iLCJzdWIiOiJhZG1pbiIsImV4cCI6MTU3MzU5Mzk0Nn0.qXBqXAelq5ywMzO7d5p7DXOKiurwPFw-BF8Sf6arelV8V7UGUrinxP8R0vpZ6SSaRNyrrS1nf9_Dgtiq0k6Tcg', 'false');

INSERT INTO user_phone(user_id, phone_id) VALUES(1,1);
