package edu.global.logic.demo.jwt.usuario;

import javax.persistence.*;

/**
 * Created by eespinoza.
 */
@Entity
//@Table(name="phone")
public class Phone {
    @SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Transient
    private Long id;

    @Column(name="number")
    private String number;

    @Column(name="citycode")
    private String citycode;
    
    @Column(name="contrycode")
    private String contrycode;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getContrycode() {
		return contrycode;
	}

	public void setContrycode(String contrycode) {
		this.contrycode = contrycode;
	}

 
}
