package edu.global.logic.demo.jwt.usuario;

import static edu.global.logic.demo.jwt.security.Constants.HEADER_AUTHORIZACION_KEY;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import edu.global.logic.demo.util.UtilDemo;
 
@RestController
public class UsuarioController {

	private UsuarioRepository usuarioRepository;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UsuarioController(UsuarioRepository usuarioRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.usuarioRepository = usuarioRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@PostMapping("/users/")
	@ResponseStatus(HttpStatus.OK)
	public Usuario saveUsuario(@RequestBody Usuario user, HttpServletRequest req) throws Exception{
		
		if(!UtilDemo.validate(user.getEmail())) {
			
			throw new Exception("formato de correo no valido");		
		}	
		
		Usuario usuario = new Usuario(); 
		List<Usuario> listUsuario = usuarioRepository.findByEmail(user.getEmail());
		if(!listUsuario.isEmpty()) {
			
			throw new Exception("correo ya registrado");
		}
			
		String header = req.getHeader(HEADER_AUTHORIZACION_KEY);
		
		String authToken = header.substring(7);
		
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setToken(authToken);
		user.setIsactive("true");
		usuarioRepository.save(user);
		
		usuario = user;
		return usuario;
	}
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        return new ResponseEntity<>(getBody(HttpStatus.INTERNAL_SERVER_ERROR, ex, "Wrong"),  new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	 public Map<String, Object> getBody(HttpStatus status, Exception ex, String message) {
     
		 Map<String, Object> body = new LinkedHashMap<>();
		 body.put("status", status.value());
		 body.put("message", ex.getMessage().toString());
		 return body;
	}
	 
	@GetMapping("/users/")
	public List<Usuario> getAllUsuarios() {
		return usuarioRepository.findAll();
	}

//	@GetMapping("/users/{username}")
//	public Usuario getUsuario(@PathVariable String username) {
//		return usuarioRepository.findByUsername(username);
//	}
}
