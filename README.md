## jwt
API REST que utiliza JWT para cumplir con lo solicitado en prueba global logic.

## Ticket validos por 10 dias generados por jwt.

eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzI3MzAzNzUsImlzcyI6Imh0dHA6Ly93d3cudGVzbGFpdC5jbC8iLCJzdWIiOiJhZG1pbiIsImV4cCI6MTU3MzU5NDM3NX0.7lfiYuH-LCQc7SNIWHnOLPLemCd1l76bGUfX_ARRuxW00a4p6-flI3WxZ7O5rz07ifZ-mSwujq6ZBErWwKCVuQ


## Damos de alta un nuevo usuario
curl -i -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NzI3MzAzNzUsImlzcyI6Imh0dHA6Ly93d3cudGVzbGFpdC5jbC8iLCJzdWIiOiJhZG1pbiIsImV4cCI6MTU3MzU5NDM3NX0.7lfiYuH-LCQc7SNIWHnOLPLemCd1l76bGUfX_ARRuxW00a4p6-flI3WxZ7O5rz07ifZ-mSwujq6ZBErWwKCVuQ" -X POST -d "{\"username\":\"admin\", \"name\":\"test 1\", \"email\":\"123@123.cl\", \"password\":\"$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.\"}" http://localhost:8080/users/
